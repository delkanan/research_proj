'''
Created on May 8, 2017

@author: davidchristian
'''
from matplotlib.mlab import PCA
import pandas as pd
import numpy

DIR = "../raw/hg19/combined.raw_expression.average.txt"

data = numpy.array(pd.read_csv(DIR, sep="\t", index_col = 0))
results = PCA(data)

print results.Y