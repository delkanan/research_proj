library("sva")

FILENAME <- "hg38.quantile.raw_expression.highest.txt"
OUTPUT <- "../normalized/hg38.combat.raw_expression.highest.txt"

data = read.csv(paste("../normalized/", FILENAME, sep = ""), sep = "\t")

samples = read.csv("../samples.txt", sep = "\t", header = FALSE, col.names = c("datasets", "chip_id", "desc", "group", "platform"))[-1]
rownames(samples) <- samples$chip_id
samples <- samples[,-1]
chip_ids <- substring(rownames(samples), 1, 10)
samples["chip_id"] <- chip_ids

batch <- samples$chip_id
modcombat <- model.matrix(~as.factor(group), data=samples)

combat_edata = ComBat(dat=data, batch=batch, mod = modcombat, par.prior = TRUE, prior.plots = FALSE)

colnames(combat_edata) <- substring(colnames(combat_edata), 2)
write.table(combat_edata, OUTPUT, sep = "\t")