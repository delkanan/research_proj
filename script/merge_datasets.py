'''
Created on May 8, 2017

@author: davidchristian
'''
import pandas as pd
import sys

RAW_DIR = "../raw/"

dataset_ids = [6555, 6557, 6561, 6687, 6688, 6689, 6690, 6691, 6692, 6701, 6702, 6704, 6706, 6707, 6709, 6715]
if len(sys.argv) > 1:
    datasets = map(int, sys.argv[1:])
    
list_data = []

sample_labels = set()

def merge_datasets(ver, opt):
    filename = RAW_DIR+ver+"/"+str(dataset_ids[0])+".raw_expression.final_"+opt+".txt"
    df = []
    for data_id in dataset_ids:
        fname = RAW_DIR+ver+"/"+str(data_id)+".raw_expression.final_"+opt+".txt"
        data = pd.read_csv(fname, sep = "\t", index_col = 0)
        df.append(data)
    result = pd.concat(df, axis=1)
    
    for col in result:
        if col not in sample_labels:
            result.drop(col, axis = 1, inplace = True)
    result.to_csv(RAW_DIR+ver+"/combined.raw_expression."+opt+".txt", sep = "\t")
    
sample_file = open("../samples.txt")
for line in sample_file:
    row = line.strip().split('\t')
    sample_labels.add(row[1])
    

for ver in ["hg19", "hg38"]:
    merge_datasets(ver, "highest")
    merge_datasets(ver, "average")