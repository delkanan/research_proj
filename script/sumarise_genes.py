'''
Created on May 6, 2017

@author: davidchristian
'''
import sys

RAW_DIR = "../raw/"

dataset_ids = [6555, 6701, 6702, 6706, 6707, 6709, 6715, 6557, 6561, 6687, 6688, 6689, 6690, 6691, 6692, 6704]
if len(sys.argv) > 1:
    datasets = map(int, sys.argv[1:])

#summarise genes expression level (highest / average)
OPT = "average"

def summarise_dataset(data_id, gene_version):
    exp_dict = {} #dict of gene expression level. key = gene name, value = list of expression level
    filename = RAW_DIR + gene_version + "/mapped/" + data_id + ".raw_expression.mapped.txt"
    with open(filename, "rU") as f:
        for line in f:
            details = line.strip().split('\t')
            key = details[1]
            if key in exp_dict:
                for idx, item in enumerate(details[2:]):
                    exp_dict[key][idx].append(item)
            else:
                exp_dict[key] = []
                for idx, item in enumerate(details[2:]):
                    exp_dict[key].append([item])
                
    
    output_name = RAW_DIR + gene_version + "/summarized/" + data_id + ".raw_expression.summarized_"+OPT+".txt"
    out = open(output_name, "w")
    for key in exp_dict:
        str_out = key
        r1 = exp_dict[key]
        
        for r2 in r1:
            assert OPT == "highest" or OPT == "average"
            if OPT == "highest":
                str_out += '\t' + str(max(r2))
            elif OPT == "average":
                str_out += '\t' + str(sum(map(float, r2))/float(len(r2)))
        str_out += "\n"
        out.write(str_out)
    out.close()


for data_id in dataset_ids:
    summarise_dataset(str(data_id), "hg19")
    summarise_dataset(str(data_id), "hg38")
            
