'''
Created on May 7, 2017

@author: davidchristian
'''

import sys

RAW_DIR = "../raw/"
OUTPUTNAME = "genes_set.txt"

datasets = [6555, 6701, 6702, 6706, 6707, 6709, 6715, 6557, 6561, 6687, 6688, 6689, 6690, 6691, 6692, 6704]

if len(sys.argv) > 1:
    datasets = map(int, sys.argv[1:])
    
def find_intersection(gene_version):
    sets = []
    
    for id in map(str, datasets):
        gene_set = set()
        with open(RAW_DIR+gene_version+"/mapped/"+id+".raw_expression.mapped.txt", "rU") as f:
            lines = f.readlines()
            for line in lines:
                key = line.strip().split('\t')[1]
                gene_set.add(key)
        sets.append(gene_set)
        print id
        print len(gene_set)
    
    
    genes_intersection = set.intersection(*sets)
    print "gene intersection"
    print len(genes_intersection)
    
    for idx, gene_set in enumerate(sets):
        gene_set = sets[idx]
        data_id = datasets[idx]
        
        complement = gene_set - genes_intersection
        print str(id) + " complement"
        print len(complement)
        
        filename = RAW_DIR + gene_version + "/complement/" + str(data_id)+".gene_complement.txt"
        output = open(filename, "w")
        for item in complement:
            output.write(item.strip() + '\n')
        output.close
        
    
    output = open(RAW_DIR+"/"+gene_version+"/" + gene_version + OUTPUTNAME, "w")
    for item in genes_intersection:
        output.write(item.strip() + '\n')
    output.close()
    
for v in ["hg19", "hg38"]:
    find_intersection(v)