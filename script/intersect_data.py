'''
Created on May 7, 2017

@author: davidchristian
'''
import sys

RAW_DIR = "../raw/"

dataset_ids = [6555, 6701, 6702, 6706, 6707, 6709, 6715, 6557, 6561, 6687, 6688, 6689, 6690, 6691, 6692, 6704]
if len(sys.argv) > 1:
    datasets = map(int, sys.argv[1:])

union_map = {"hg19" : set(), "hg38" : set()}

#highest/average
OPT = "highest"

def init_set(version):
    with open(RAW_DIR+version+"/"+version+"genes_set.txt") as f:
        for line in f:
            union_map[version].add(line.strip())

def intersect_output(version, data_id):
    ori_f = open(RAW_DIR+data_id+".raw_expression.txt")
    header = ori_f.readline()
    
    with open(RAW_DIR+version+"/summarized/"+data_id+".raw_expression.summarized_"+OPT+".txt") as f:
        output = open(RAW_DIR+version+"/"+data_id+".raw_expression.final_"+OPT+".txt", "w")
        comp = open(RAW_DIR+version+"/complement/"+data_id+".raw_expression.complement_"+OPT+".txt", "w")
        output.write(header)
        comp.write(header)
        for line in sorted(f):
            row = line.strip().split("\t")
            if row[0] in union_map[version]:
                output.write(line)
            else:
                comp.write(line)
        output.close()
        comp.close()

for version in ["hg19", "hg38"]:
    init_set(version)

for data_id in dataset_ids:
    intersect_output("hg19", str(data_id))
    intersect_output("hg38", str(data_id))
