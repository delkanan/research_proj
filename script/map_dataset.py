'''
Created on May 6, 2017

@author: davidchristian
'''

RAW_DIR = "../raw/"

dataset_ids = {"ht12v3" : [6555, 6701, 6702, 6706, 6707, 6709, 6715],
               "ht12v4" : [6557, 6561],
               "wg6v2" : [6687, 6688, 6689, 6690, 6691, 6692, 6704]}
# dataset_ids = {"ht12v3" : [6555]}

mapping = {"hg19ht12v3" : {},
           "hg19ht12v4" : {},
           "hg19wg6v2" : {},
           "hg38ht12v3" : {},
           "hg38ht12v4" : {},
           "hg38wg6v2" : {}}

#generate map
def generate_map(gene_version, platform):
    gp = gene_version + platform
    temp_map = mapping[gp]
    filename = "../probes_mapping/" + gp + "_gene_mapping.unique.txt"
    
    with open(filename, "rU") as f:
        for line in f:
            data = line.strip().strip().split('\t')
            
            assert data[0] not in temp_map
            temp_map[data[0]] = data[2]

def output_probes_mapping(dataset_id, platform, gene_version):
    with open(RAW_DIR+dataset_id+".raw_expression.txt") as f:
        output = open(RAW_DIR+gene_version+"/mapped/"+dataset_id+".raw_expression.mapped.txt", "w")
        unmapped = open(RAW_DIR+gene_version+"/mapped/"+dataset_id+".raw_expression.unmapped.txt", "w")
        
        next(f)
        for line in f:
            row = line.strip().split('\t')
            if row[0] in mapping[gene_version+platform]:
                out = row[0] +"\t"+ mapping[gene_version+platform][row[0]]+"\t"
                for r in row[1:]:
                    out = out + r + "\t"
                output.write(out[:-1]+"\n")
            else:
                unmapped.write(line)
        output.close()
        unmapped.close()
            
    
for key in dataset_ids:
    generate_map("hg19", key)
    generate_map("hg38", key)
    
#map dataset
for key in dataset_ids:
    for dataset_id in dataset_ids[key]:
        output_probes_mapping(str(dataset_id), key, "hg19")
        output_probes_mapping(str(dataset_id), key, "hg38")
        
                
            