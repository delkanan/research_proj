'''
Created on May 5, 2017

@author: davidchristian
'''

import pandas as pd
import sys

input = "hg19ht12v3"

if len(sys.argv > 1):
    input = sys.argv[1]

#mapping files are generated from ensembl biomart
mapping = pd.read_csv("../probes_mapping/"+input+"_gene_mapping.txt", sep="\t")
colnames = ["probeID", "geneID", "geneName", "geneType"]
mapping.columns = colnames

mapping.drop_duplicates(inplace=True)
mapping.sort_values("probeID", inplace=True)

#drop duplicates
unique_map = mapping.drop_duplicates(subset=["probeID"], keep=False)

multimap = mapping[-mapping.index.isin(unique_map.index)]
multimap_probes = multimap["probeID"].unique()

#list to store multimapping probes that mapped to same gene name
same_mapping = pd.DataFrame(columns=colnames)
i=0
for probe in multimap_probes:
    m = multimap[multimap["probeID"] == probe]
    
    if len(m.drop_duplicates(subset = ["probeID", "geneName"])) == 1:
        probeID = m.iloc[0]["probeID"]
        geneID = ""
        geneName = m.iloc[0]["geneName"]
        geneType = m.iloc[0]["geneType"]
        
        for index, row in m.iterrows():
            geneID = geneID+row.geneID+"/"
        add_row = [probeID, geneID[:-1], geneName, geneType]
        same_mapping.loc[i] = add_row
        i += 1

final_mapping = pd.DataFrame(columns=colnames).append(unique_map).append(same_mapping)
print final_mapping

final_mapping.to_csv("../probes_mapping/"+input+"_gene_mapping.unique.txt", sep="\t", index = False)
multimap.to_csv("../probes_mapping/"+input+"_gene_mapping.multi.txt", sep="\t", index = False)
multimap.to_csv("../probes_mapping/"+input+"_gene_mapping.multi_same_gene.txt", sep="\t", index = False)

